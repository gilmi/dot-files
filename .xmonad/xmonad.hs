{-# LANGUAGE TupleSections #-}

import XMonad
import XMonad.Hooks.DynamicLog
import XMonad.Hooks.ManageDocks
import XMonad.Util.Run(spawnPipe)
import XMonad.Util.EZConfig (additionalKeys, mkNamedKeymap)
import XMonad.Util.NamedActions (addName, addDescrKeys, NamedAction(..), showKm)
import XMonad.Actions.Navigation2D (windowGo, windowSwap, screenGo, windowToScreen, screenSwap)
import XMonad.Actions.MessageFeedback (tryMessage_)
import XMonad.Util.Cursor (setDefaultCursor)
import XMonad.Operations (sendMessage)
import XMonad.Layout.BinarySpacePartition (ResizeDirectional(ExpandTowards))
import XMonad.Layout.ThreeColumns
import XMonad.Layout.Spacing
import XMonad.Layout.IfMax
import XMonad.Layout.Dwindle
import XMonad.Layout.IndependentScreens
import XMonad.Layout.Fullscreen hiding (fullscreenEventHook)
import XMonad.Layout.NoBorders
import XMonad.Layout.Reflect (Reflect)
import XMonad.Hooks.ManageHelpers (isFullscreen, isDialog, doFullFloat)
import XMonad.Hooks.EwmhDesktops (fullscreenEventHook, ewmh)
import XMonad.Config.Desktop (desktopConfig)

import System.IO

-- solution for steam games: https://bbs.archlinux.org/viewtopic.php?pid=1716964#p1716964

myModKey = mod4Mask

-- Display keyboard mappings using zenity
-- from https://github.com/thomasf/dotfiles-thomasf-xmonad/
--              blob/master/.xmonad/lib/XMonad/Config/A00001.hs
showKeybindings :: [((KeyMask, KeySym), NamedAction)] -> NamedAction
showKeybindings x = addName "Show Keybindings" $ io $ do
  h <- spawnPipe "zenity --text-info --font=terminus"
  hPutStr h (unlines $ showKm x)
  hClose h
  pure ()

main :: IO ()
main = do
  xmproc <- spawnPipe "killall -9 xmobar; LC_CTYPE=en_IN.utf8 /home/suppi/.local/bin/xmobar /home/suppi/.xmonad/.xmobarrc"
  numOfScreens <- countScreens

  let
    conf =
      def
      { manageHook = composeAll $
          [ manageDocks
          , manageHook def
          , isDialog --> doFloat
          , isFullscreen --> doFullFloat
          ]

      , layoutHook = avoidStruts $ myLayout

      , logHook = dynamicLogWithPP xmobarPP
          { ppOutput = hPutStrLn xmproc
          , ppTitle = xmobarColor "#ff88cc" "" . shorten 50
          }

      , modMask = myModKey     -- Rebind Mod to the Windows key

      , focusedBorderColor = "#2aa8fc"

      , terminal = "xfce4-terminal"

      , startupHook = myStartupHook

      , handleEventHook =
        handleEventHook desktopConfig <+> fullscreenEventHook
      }

  xmonad
    . ewmh
    . addDescrKeys ((myModKey, xK_F1), showKeybindings) myKeys
    . docks
    $ conf

myKeys conf =
  let
    dirKeys        = ["k","l","j",";"]
    arrowKeys      = ["<D>","<U>","<L>","<R>"]
    dirs           = [ D,  U,  L,  R ]

    zipM' m nm ks as f b = zipWith (\k d -> (m ++ k, addName nm $ f d b)) ks as
    tryMsgR x y = sequence_ [(tryMessage_ x y), refresh]

  in mkNamedKeymap conf $
    [ ("M-[", addName "Expand (L on BSP)" $ tryMsgR (ExpandTowards L) Shrink)
    , ("M-]", addName "Shrink (R on BSP)" $ tryMsgR (ExpandTowards R) Expand)

    , ("<Print>", addName "Take screenshot" $ spawn "scrot -q 1 /home/suppi/Pictures/screenshots/%Y-%m-%d-%H:%M:%S.png")
    , ("<XF86AudioLowerVolume>", addName "Lower volume" $ spawn "amixer -D pulse sset Master 2%-")
    , ("<XF86AudioRaiseVolume>", addName "Raise volume" $ spawn "amixer -D pulse sset Master 2%+")
    , ("<XF86AudioNext>", addName "Audio next"  $ spawn "cmus-remote --next")
    , ("<XF86AudioPrev>", addName "Audio prev"  $ spawn "cmus-remote --prev")
    , ("<XF86AudioPlay>", addName "Audio toggle play" $ spawn "cmus-remote --pause")
    , ("M-e", addName "Emacs" $ spawn "emacs")
    , ("M-h", addName "M-h" $ pure ())
    ]

    ++ zipM' "M-" "Navigate window" dirKeys dirs windowGo True
    ++ zipM' "M-S-" "Move window" dirKeys dirs windowSwap True
    ++ zipM' "M-" "Navigate screen" arrowKeys dirs screenGo True
    ++ zipM' "M-C-" "Move window to screen" arrowKeys dirs windowToScreen True
    ++ zipM' "M-S-" "Swap workspace to screen" arrowKeys dirs screenSwap True

myStartupHook =
  --spawn "compton --backend glx --vsync opengl -fcCz -l -17 -t -17" --shadow-red 0.35 --shadow-green 0.92 --shadow-blue 0.93" --f
  spawn "killall -9 compton; compton --backend glx --vsync opengl"
  --spawn "compton --backend glx -f" --f
  <+> setDefaultCursor xC_pirate

myLayout =
  spacingWithEdge 8
  (     IfMax 4 (Tall 1 (3/100) (1/2)) (Dwindle R CW 1.5 1.1)
    ||| IfMax 4 (Tall 2 (3/100) (1/2)) (Dwindle R CW 1.5 1.1)
    ||| Mirror (Tall 1 (3/100) (1/2))
    ||| ThreeColMid 1 (3/100) (1/2)
  ) ||| noBorders Full
