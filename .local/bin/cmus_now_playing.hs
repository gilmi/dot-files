#!/usr/bin/env stack
-- stack --resolver lts-16.15 script --package text --package process


{-# LANGUAGE OverloadedStrings #-}

import Control.Monad
import qualified Data.Text as T
import qualified Data.Text.IO as T
import System.Process
import Control.Exception
import Control.Concurrent


main :: IO ()
main = forever $ do
  run `catch` \(SomeException e) -> pure ()
  threadDelay 2000000

run :: IO ()
run = do
  current <- readTextFile
  text <- getSong

  unless (current == text) $ do
    deleteFromFile current
    threadDelay delay
    writeToFile "" =<< getSong

deleteFromFile :: T.Text -> IO ()
deleteFromFile current = do
  unless (T.null current) $ do
    let
      temp
        | T.drop (T.length current - 2) current == " " =
          T.take (T.length current - 2) current
        | otherwise =
          T.take (T.length current - 1) current
    T.writeFile file (prefix <> temp)
    threadDelay delay
    deleteFromFile temp

writeToFile :: T.Text -> T.Text -> IO ()
writeToFile current full = do
  unless (current == full) $ do
    let
      temp
        | T.drop (T.length current) full == " " =
          T.take (T.length current + 2) full
        | otherwise =
          T.take (T.length current + 1) full
    T.writeFile file (prefix <> temp)
    threadDelay delay
    writeToFile temp full

getSong :: IO T.Text
getSong =
  fmap
    (T.unwords . T.lines . T.pack)
    (readCreateProcess (shell "cmus-remote -C \"format_print %A - %t\"") "")

readTextFile :: IO T.Text
readTextFile =
  catch
    (T.drop (T.length prefix) <$> T.readFile file)
    (\(SomeException _) -> pure "")

file :: FilePath
file = "/tmp/cmus_playing.txt"

prefix :: T.Text
prefix = "Now Playing: "

delay = 95000

