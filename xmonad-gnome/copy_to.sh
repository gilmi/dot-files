#!/usr/bin/bash
cp xmonad.desktop.apps /usr/share/applications/xmonad.desktop
cp xmonad.desktop.xsses /usr/share/xsessions/xmonad.desktop
cp xmonad.session /usr/share/gnome-session/sessions/xmonad.session
cp gnome-session-xmonad /usr/bin/gnome-session-xmonad
cp gnome-session-xmonad /home/suppi/gnome-session-xmonad
