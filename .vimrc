syntax on
scriptencoding utf-8

set laststatus=2
set backspace=indent,eol,start
set nowrap        " don't wrap long lines
set autoindent
set shiftwidth=4  " 4 spaces for indentation
set expandtab     " spaces instead of tabs
set tabstop=4
set softtabstop=4
set nu

set nospell " spell checking

set showmode

set showmatch    " show matching bracket
set hlsearch     " highlight search terms
set incsearch    " find as you type search
set ignorecase   " case insensitive search
set smartcase    " case sensitive when uc present

set scrolljump=0
set scrolloff=3

colorscheme elflord

nnoremap j h
nnoremap k j
nnoremap l k
nnoremap ; l
vnoremap j h
vnoremap k j
vnoremap l k
vnoremap ; l

let mapleader=" "

nnoremap <F5> ^i-- <esc>A --<esc>yypVr-yykP
