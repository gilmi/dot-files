export PATH="/home/suppi/.local/bin:$PATH"
export PATH="/home/suppi/tools/purs-0.12:$PATH"

export PATH="/home/suppi/.cargo/bin:$PATH"
export PATH="/home/suppi/tools/racket/bin:$PATH"

alias ghcis="ghci"
alias ghcs="stack exec -- ghc"
alias runghcs="stack exec -- runghc"
alias runhaskells="runghc"

alias ghc="stack exec -- ghc"
alias ghci="stack exec -- ghci"
