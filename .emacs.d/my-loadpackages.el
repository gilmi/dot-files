; my-loadpackages.el
; loading package

;;; Code:

(load "~/.emacs.d/my-packages.el")

;; evil mode

(require 'magit)

(setq evil-want-integration t) ;; This is optional since it's already set to t by default.
(setq evil-want-keybinding nil)
(require 'evil)
(evil-mode 1)

(with-eval-after-load 'evil-maps
  (define-key evil-motion-state-map (kbd "SPC") nil)
  (define-key evil-visual-state-map (kbd "SPC") nil)
  (define-key evil-motion-state-map (kbd "RET") nil)
  (define-key evil-visual-state-map (kbd "RET") nil)
  (define-key evil-motion-state-map (kbd "TAB") nil)
  (define-key evil-visual-state-map (kbd "TAB") nil)

  (define-key evil-normal-state-map (kbd "C-.") nil)
  (define-key evil-normal-state-map (kbd "M-.") nil)
  (define-key evil-motion-state-map "h" nil)
  (define-key evil-motion-state-map "j" nil)
  (define-key evil-motion-state-map "k" nil)
  (define-key evil-motion-state-map "l" nil)
  (define-key evil-motion-state-map "j" 'evil-backward-char)
  (define-key evil-motion-state-map "k" 'evil-next-line)
  (define-key evil-motion-state-map "l" 'evil-previous-line)
  (define-key evil-motion-state-map ";" 'evil-forward-char)
  (define-key evil-motion-state-map "q" nil)
  (define-key evil-normal-state-map "q" nil)
)

(require 'evil-collection)
(when (require 'evil-collection nil t)
  (evil-collection-init '(calendar magit calc ediff)))

(evil-define-key evil-collection-magit-state magit-mode-map "k" 'evil-next-visual-line)
(evil-define-key evil-collection-magit-state magit-mode-map "l" 'evil-previous-visual-line)
(evil-define-key evil-collection-magit-state magit-mode-map "j" 'evil-backward-char)

;;

(require 'which-key)
(which-key-mode)

(require 'avy)

(global-set-key (kbd "M-s") 'avy-goto-word-1)

(require 'popwin)
(popwin-mode 1)

(global-set-key [f7] 'buffer-menu)

(require 'neotree)
(global-set-key [f8] 'neotree-toggle)

(require 'imenu-list)
(global-set-key [f6] 'imenu-list-smart-toggle)
(setq imenu-list-idle-update-delay 0.3)
(setq imenu-list-size 0.2)

;; color theme

(load-theme 'seti t)
(custom-theme-set-faces
 'seti
 ; `(font-lock-comment-face ((t (:foreground ,"#9d8691"))))
 `(font-lock-comment-face ((t (:foreground ,"#998896"))))
)

; (load-theme 'whiteboard t)

;; auto completion

(add-hook 'after-init-hook 'global-company-mode)
(global-set-key (kbd "C-c w") 'company-complete)

(require 'company-box)
(add-hook 'company-mode-hook 'company-box-mode)

(setq company-minimum-prefix-length 1)
;(setq company-dabbrev-downcase 0)
(setq company-idle-delay 0.5)

;; flycheck

(require 'flycheck)

(add-hook 'after-init-hook #'global-flycheck-mode)
(require 'flycheck-color-mode-line)

(require 'flycheck-pos-tip)
(with-eval-after-load 'flycheck (flycheck-pos-tip-mode))
(setq flycheck-pos-tip-timeout 30)

;;;


(with-eval-after-load 'flycheck
  (setq-default flycheck-disabled-checkers '(emacs-lisp-checkdoc)))

(require 'flycheck-color-mode-line)
(add-hook 'flycheck-mode-hook
  'flycheck-color-mode-line-mode)

(global-set-key [f9] 'flycheck-list-errors)

(setq flycheck-color-mode-line-show-running nil)

;; indent guide

(require 'indent-guide)
(indent-guide-global-mode)

;;;;;;;;;;;;;
;;         ;;
;; HASKELL ;;
;;         ;;
;;;;;;;;;;;;;

; (require 'attrap)


;;;;;;;; LSP

;(require 'eglot)

;(add-hook 'haskell-mode-hook 'eglot-ensure)


; (require 'lsp-ui)
; (require 'lsp-treemacs)
; (lsp-treemacs-sync-mode 1)
; (require 'lsp-mode)
; (require 'lsp-haskell)
; (add-hook 'haskell-mode-hook #'lsp)
; (add-hook 'haskell-literate-mode-hook #'lsp)
;
; (custom-set-variables
;  '(lsp-haskell-brittany-on nil)
;  '(lsp-haskell-eval-on nil)
;  '(lsp-haskell-floskell-on nil)
;  '(lsp-haskell-formatting-provider "none")
;  '(lsp-haskell-fourmolu-on nil)
;  '(lsp-haskell-ghcide-on nil)
;  '(lsp-haskell-hlint-on nil)
;  '(lsp-haskell-importlens-on nil)
;  ; '(lsp-haskell-modulename-on nil)
;  ; '(lsp-haskell-ormolu-on nil)
;  ; '(lsp-haskell-pragmas-on nil)
;  ; '(lsp-haskell-refineimports-on nil)
;  '(lsp-haskell-retrie-on nil)
;  '(lsp-haskell-server-args (quote ("-d" "-l" "/tmp/hls.log" "+RTS -M4g -RTS")))
;  '(lsp-haskell-stylish-haskell-on nil)
;  '(lsp-haskell-tactic-on nil)
;  '(lsp-ui-doc-alignment (quote window))
;
;  '(lsp-haskell-formatting-provider "ormolu")
;  '(lsp-haskell-fourmolu-on 't)
;  '(lsp-ui-doc-alignment 'window)
;  '(lsp-ui-doc-delay 0.2)
;  '(lsp-ui-doc-include-signature t)
;  '(lsp-ui-doc-position 'top)
;  '(lsp-ui-doc-text-scale-level 2)
;  '(lsp-ui-sideline-show-hover nil))

;; command map

; (define-key lsp-command-map (kbd "M-.") 'lsp-find-definition)

;;;;;;; dante


(require 'dante)
(require 'haskell-mode)

(add-hook 'haskell-mode-hook 'dante-mode)
(setq dante-tap-type-time 1)

(define-key haskell-mode-map (kbd "C-c C-`") 'haskell-interactive-bring)
(define-key haskell-mode-map (kbd "C-l C-l") 'haskell-process-load-or-reload)
(define-key haskell-mode-map (kbd "C-c C-k") 'haskell-interactive-mode-clear)

;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;
;; (custom-set-variables																										   ;;
;;   '(dante-methods-alist																										   ;;
;;    `(																														   ;;
;;      (stack "stack.yaml" ("stack" "repl" dante-target))																		   ;;
;;      (new-build "cabal.project.local" ("mycabal" "v2-repl" (or dante-target (dante-package-name) nil) "--builddir=dist/dante")) ;;
;;      (new-build "cabal.project.local" ("cabal" "new-repl" (or dante-target (dante-package-name) nil) "--builddir=dist/dante"))  ;;
;;      (bare-cabal ,(lambda (d) (directory-files d t "..cabal$")) ("cabal" "v2-repl" dante-target "--builddir=newdist/dante"))	   ;;
;;      (bare-stack ,(lambda (_) t) ("stack ghci"))																				   ;;
;;      (bare-ghci ,(lambda (_) t) ("ghci")))))																					   ;;
;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;;

; (custom-set-variables
;   '(dante-methods-alist
;    `(
;      (new-build "cabal.project" ("mycabal" "repl" (or dante-target (dante-package-name) nil) "--builddir=dist/dante")) ; -------- adding this
;      (new-build "cabal.project.local" ("cabal" "new-repl" (or dante-target (dante-package-name) nil) "--builddir=dist/dante"))
;      (stack "stack.yaml" ("stack" "repl" dante-target))
;      (bare-cabal ,(lambda (d) (directory-files d t "..cabal$")) ("cabal" "v2-repl" dante-target "--builddir=newdist/dante"))
;      (bare-ghci ,(lambda (_) t) ("ghci")))))


;;; search work under the cursor in hoogle

(defun haskell-search-hoogle ()
  "Search hoogle for the word under the cursor"
  (interactive)
  (browse-url-generic (concat "http://hoogle.haskell.org/?hoogle=" (thing-at-point 'word))))

(define-key haskell-mode-map (kbd "C-:") 'haskell-search-hoogle)

(setq browse-url-generic-program (executable-find "firefox"))

;;; other modes

(require 'rust-mode)
(require 'lsp-mode)
(add-hook 'rust-mode-hook #'lsp)
(add-hook 'rust-mode-hook 'cargo-minor-mode)
(add-hook 'flycheck-mode-hook #'flycheck-rust-setup)
(setq rust-format-on-save t)

(require 'irony)

;; (add-hook 'irony-mode '(setq irony-compile-flags '("-I/usr/include/SDL2")))
(add-hook 'irony-mode '(setq irony-additional-clang-options '("-std=c++17")))
(add-hook 'c++-mode-hook (lambda () (setq flycheck-gcc-language-standard "c++17")))
(add-hook 'c++-mode-hook (lambda () (setq flycheck-clang-language-standard "c++17")))

(setq irony--compile-options
      '("-std=c++17"))

;;; emacs lisp

(require 'paredit)
(require 'evil-paredit)

(add-hook
  'emacs-lisp-mode-hook
  'paredit-mode
  'evil-paredit-mode
  )
(add-hook 'emacs-lisp-mode-hook #'rainbow-delimiters-mode)

;;; org-mode

(require 'org)
(require 'toc-org nil t)
(add-hook 'org-mode-hook 'toc-org-enable)
(add-hook 'org-mode-hook 'visual-line-mode)

(setq org-latex-packages-alist '(("margin=2cm" "geometry" nil)))

(setq org-src-fontify-natively t)

(setcar (nthcdr 2 org-emphasis-regexp-components) " \t\r\n,\"")

(defun org-haskell-code ()
  "Will insert an org-mode code block with haskell highlight"
  (interactive)
  (progn
    (insert "#+BEGIN_SRC haskell" "\n\n" "#+END_SRC")
    (forward-line -1)))

(define-key org-mode-map (kbd "C-c h") 'org-haskell-code)

;;; Nix

(require 'nix-mode)
(add-to-list 'auto-mode-alist '("\\.nix\\'" . nix-mode))

(set-variable 'shell-command-switch "-c")

;; SQL format
(defun sqlparse-region (beg end)
  (interactive "r")
  (shell-command-on-region
   beg end
   "/home/gilmi/.local/bin/sqlformat"
   t t))

;;; Giml

; (load "~/code/giml/giml-lang/tools/emacs/giml-mode.el")
