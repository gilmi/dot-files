(require 'cl)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "https://melpa.org/packages/") t)
(package-initialize)

; defvar is the correct way to declare global variables
; ; you might see setq as well, but setq is supposed to be use just to set variables,
; ; not create them.
(defvar required-packages
  '(
    evil
    popwin
    avy
    toc-org
    which-key
    imenu-list
    indent-guide

    neotree
    magit
    evil-collection

    seti-theme

    popup
    company
    flycheck
    flycheck-pos-tip
    flycheck-color-mode-line

	haskell-mode
    attrap
    dante

	; rust
	rust-mode
	cargo
	flycheck-rust
	lsp-mode
	lsp-ui
	lsp-treemacs

	;eglot
    company-box

    markdown-mode
    markdown-toc

    irony
    flycheck-irony

    paredit
    evil-paredit
    rainbow-delimiters

    htmlize
    yaml-mode

    nix-mode
    )
   "a list of packages to ensure are installed at launch.")


; method to check if all packages are installed
(defun packages-installed-p ()
  (cl-loop for p in required-packages
        when (not (package-installed-p p)) do (cl-return nil)
        finally (cl-return t)))

; if not all packages are installed, check one by one and install the missing ones.
(unless (packages-installed-p)
  ; check for new packages (package versions)
  (message "%s" "Emacs is now refreshing its package database...")
  (package-refresh-contents)
  (message "%s" " done.")
  ; install the missing packages
  (dolist (p required-packages)
    (when (not (package-installed-p p))
      (package-install p))))
