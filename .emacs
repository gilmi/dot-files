;;; giml

; (load "~/code/giml/giml-lang/tools/emacs/giml-mode.el")


;; .emacs

; https://github.com/hlissner/doom-emacs/blob/develop/docs/faq.org#how-does-doom-start-up-so-quickly
(setq gc-cons-threshold most-positive-fixnum ; 2^61 bytes
      gc-cons-percentage 0.6)

(add-hook 'emacs-startup-hook
  (lambda ()
    (setq gc-cons-threshold 16777216 ; 16mb
          gc-cons-percentage 0.1)))


;;; Code:

;; set unicode everywhere
(set-charset-priority 'unicode)
(setq locale-coding-system 'utf-8)
(set-terminal-coding-system 'utf-8)
(set-keyboard-coding-system 'utf-8)
(set-selection-coding-system 'utf-8)
(prefer-coding-system 'utf-8)
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))
;; avoid backup files
(setq
  make-backup-files nil
  auto-save-default nil
  create-lockfiles nil)
;; start screen
(setq inhibit-startup-screen t)

;; force emacs to use ~/.bashrc path and aliases
(let ((path (shell-command-to-string ". ~/.bashrc; echo -n $PATH")))
  (setenv "PATH" path)
  (setq exec-path
        (append
         (split-string-and-unquote path ":")
         exec-path)))

;; Added by Package.el.  This must come before configurations of
;; installed packages.  Don't delete this line.  If you don't want it,
;; just comment it out by adding a semicolon to the start of the line.
;; You may delete these explanatory comments.
;(package-initialize)

;; disable automatic description as this is both annoying and can easily
;; get intero stuck
(global-eldoc-mode -1)

(add-hook 'minibuffer-setup-hook
    (lambda () (setq truncate-lines nil)))

(setq resize-mini-windows t) ; grow and shrink as necessary
(setq max-mini-window-height 50) ; grow up to max of 50 lines

(setq minibuffer-scroll-window t)

(add-to-list 'exec-path
  "/usr/local/bin"
)
(add-to-list 'exec-path
  "~/.local/bin"
)

(add-to-list 'exec-path
  "~/.ghcup/bin"
)

(add-to-list 'exec-path
  "~/.ghcup/bin"
)

(add-hook 'eshell-mode-hook 'eshell-mode-hook-func)

;; I want M-! (shell-command) to pick up my aliases and so run .bashrc:
;(setq shell-file-name "/home/suppi/shell-for-emacs.sh")  ; Does this: BASH_ENV="~/.bashrc" exec bash "$@"
(setq shell-command-switch "-ic")

;; load packages
(load "~/.emacs.d/my-loadpackages.el")

(defun pbcopy ()
    (interactive)
      (call-process-region (point) (mark) "pbcopy")
        (setq deactivate-mark t))

(defun pbpaste ()
    (interactive)
      (call-process-region (point) (if mark-active (mark) (point)) "pbpaste" t t))

(defun pbcut ()
    (interactive)
      (pbcopy)
        (delete-region (region-beginning) (region-end)))

(global-set-key (kbd "C-c c") 'pbcopy)
(global-set-key (kbd "C-c v") 'pbpaste)
(global-set-key (kbd "C-c x") 'pbcut)

;; cycle through buffers with Ctrl-Tab
(global-set-key (kbd "<C-tab>") 'other-window)

(global-set-key (kbd "M-j") 'windmove-left)   ; move to left window
(global-set-key (kbd "M-;") 'windmove-right)  ; move to right window
(global-set-key (kbd "M-l") 'windmove-up)     ; move to upper window
(global-set-key (kbd "M-k") 'windmove-down)   ; move to lower window

(global-set-key (kbd "C-~") 'next-buffer)
(global-set-key (kbd "C-`") 'previous-buffer)

(global-set-key (kbd "M-ף") 'eval-expression)

;;; uncomment this line to disable loading of "default.el" at startup
;; (setq inhibit-default-init t)

;; enable visual feedback on selections
;(setq transient-mark-mode t)

;; default to better frame titles
(setq frame-title-format
      (concat  "%b - emacs@" (system-name)))

;; default to unified diffs
(setq diff-switches "-u")

;; always end a file with a newline
;(setq require-final-newline 'query)

;;; uncomment for CJK utf-8 support for non-Asian users
;; (require 'un-define)

;; line numbers
(display-line-numbers-mode 1)

;; TABS AND INDENTATION ;;
;; https://dougie.io/emacs/indentation/

(defun disable-tabs ()
  (interactive)
  (setq indent-tabs-mode nil))

(defun enable-tabs  ()
  (interactive)
  (local-set-key (kbd "TAB") 'tab-to-tab-stop)
  (setq indent-tabs-mode t)
  (setq tab-width 2))

(setq-default electric-indent-inhibit t)
(setq backward-delete-char-untabify-method 'hungry)
(setq-default evil-shift-width 2)

(setq-default c-basic-offset 2
              tab-width 2
              indent-tabs-mode nil)

(disable-tabs)

; (setq c-basic-indent 4)
; (setq-default tab-width 4)
; (setq tab-width 4)
; (setq-default indent-tabs-mode nil)
; (setq indent-tabs-mode nil)

; (defun use-tabs ()
; (interactive)
; (setq indent-tabs-mode t))

; (defun use-spaces ()
; (interactive)
; (setq indent-tabs-mode nil))

;; auto indent
(define-key global-map (kbd "RET") 'newline-and-indent)

(define-key global-map (kbd "<f10>") 'revert-buffer)

(setq-default cursor-in-non-selected-windows nil)

;; font
;(set-face-attribute 'default nil :height 120)

(set-frame-font "Iosevka Fixed-14")
(setq default-process-coding-system '(utf-8-unix . utf-8-unix))

(defun enlarge-font ()
  (interactive)
  (set-frame-font "Iosevka Fixed-20")
)

(define-key global-map (kbd "C-c C-7") 'enlarge-font)

(defun shrink-font ()
  (interactive)
  (set-frame-font "Iosevka Fixed-14"))
(define-key global-map (kbd "C-c C-6") 'shrink-font)

;; window size

(add-to-list 'default-frame-alist '(width . 140))
(add-to-list 'default-frame-alist '(height . 40))
(add-to-list 'default-frame-alist '(font . "Iosevka Fixed-14"))

(defun hide-menus ()
  (interactive)
  (menu-bar-mode -1)
  (toggle-scroll-bar -1)
  (tool-bar-mode -1))

(defun show-menus ()
  (interactive)
  (menu-bar-mode 1)
  (toggle-scroll-bar 1)
  (tool-bar-mode 1))

;; hide menus
(tool-bar-mode -1)

;; scrolling
(setq scroll-step 1
   scroll-conservatively  10000)

;; ido mode for narrowing lists and such
(ido-mode 1)
(setq ido-enable-flex-matching t)
(setq ido-everywhere t)

;; parens
(show-paren-mode 1)

;; delete trailing spaces
(add-hook 'before-save-hook #'delete-trailing-whitespace)
(setq require-final-newline t)

;; Warn before you exit emacs!
(setq confirm-kill-emacs 'yes-or-no-p)

;; Don't insert instructions in the *scratch* buffer
(setq initial-scratch-message "")
(setq initial-major-mode 'org-mode)

;; make all "yes or no" prompts show "y or n" instead
(fset 'yes-or-no-p 'y-or-n-p)

;; I use version control, don't annoy me with backup files everywhere
(setq make-backup-files nil)
(setq auto-save-default nil)


;; Transparency
; (set-frame-parameter (selected-frame) 'alpha '(93 . 83))
; (add-to-list 'default-frame-alist '(alpha . (93 . 83)))

(defun toggle-transparency ()
  (interactive)
  (let ((alpha (frame-parameter nil 'alpha)))
    (set-frame-parameter
     nil 'alpha
     (if (eql (cond ((numberp alpha) alpha)
                    ((numberp (cdr alpha)) (cdr alpha))
                    ;; Also handle undocumented (<active> <inactive>) form.
                    ((numberp (cadr alpha)) (cadr alpha)))
              100)
         '(85 . 50) '(100 . 100)))))
(global-set-key (kbd "C-c t") 'toggle-transparency)

;; Set transparency of emacs
(defun transparency (value)
  "Sets the transparency of the frame window. 0=transparent/100=opaque"
  (interactive "nTransparency Value 0 - 100 opaque:")
  (set-frame-parameter (selected-frame) 'alpha value))

(defun show-notification (notification)
  "Show notification. Use notify-send."
  (start-process "notify-send" nil "notify-send" "-i" "/usr/share/icons/hicolor/32x32/apps/emacs.png" notification)
)

(defun notify-compilation-result (buffer msg)
  "Notify that the compilation is finished"
  (if (equal (buffer-name buffer) "*compilation*")
    (if (string-match "^finished" msg)
      (show-notification "Compilation Successful")
      (show-notification "Compilation Failed")
    )
  )
)

(add-to-list 'compilation-finish-functions 'notify-compilation-result)

(menu-bar-mode t)
